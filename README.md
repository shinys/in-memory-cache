
## 어플리케이션 개요 
> In-Memory Cached 상품 정보 제공 Rest API 서버.

본 과제의 주 목적은 간단한 캐시를 구현하여 일련의 요구 사항을 만족하는 API 서버를 작성하는것이다.  
캐시는 `com.shinys.cache` 패키지에 구현되어있으며, 어플리케이션 코드에서는 직접 캐시를 핸들링하는게 아닌
AOP 방식으로 접근한다.  

~~~java  
@Cache(ttl = 30000L) //아래 메소드 결과를 메소드명:arg1:arg2.. 형식의 key로 캐싱
public List<Category> categoryList(){
    return dataProvidor.categoryList();
}
~~~

초기 캐싱 데이터 로딩은 `com.shinys.cache.ApplicationEvent` 에서 수행.  

~~~java
List<Category> categoryList = iCategoryRepository.findByDepth(1);
cacheManager.put("categoryList", categoryList, 20000L);

iProductRepository.findAll().forEach(p->{
    cacheManager.put("product".concat(":").concat(String.valueOf(p.getProductNo())), p,30000L);
});
~~~

## 과제 코맨트 위치  
#### case 1.  
~~~java
/**
 * case 1. Query
 * 	클라이언트 코드는 Cache Service가 제공하는 API를 통하여, 카테고리 리스트를 조회할 수 있어야 한다.
 * 	특정 카테고리에 속한 상품 리스트를 조회할 수 있어야 한다.
 * 	특정 상품에 대하여 상품명, 카테고리, 가격을 조회할 수 있어야 한다.
 *
 * case 2. Data Loading and Reloading
 * 	Cache Miss가 발생하면 적절한 시점에 cache는 스스로 해당 부분의 데이터를 원본 데이터베이스로부터 reloading한다.
 *
 * @author shinys
 */
@Service
@RequiredArgsConstructor
public class RetrieveUsecase {
~~~
#### case 2.  
~~~java
    /**
     * case 2. Data Loading and Reloading
     * 	Cache Miss가 발생하면 적절한 시점에 cache는 스스로 해당 부분의 데이터를 원본 데이터베이스로부터 reloading한다.
     *
     *  @Cache 애노테이션처리 부분에서 캐시 miss발생 시 원본 데이터 취득 처리
     *
     * @param joinPoint
     * @param cache
     * @return
     * @throws Throwable
     */
    @Around("@annotation(cache)")
    public Object cacheProcess(ProceedingJoinPoint joinPoint, Cache cache)throws Throwable{
~~~


#### case 3.  
~~~java
    CacheManager(){
        /**
         * case 3. Cache Data Eviction Policy
         * 	다양한 이유로 Cache 내부의 데이터는 Eviction 처리가 되어야 한다.
         * 	어떤 경우에 데이터 Eviction을 발생시킬 것인지를 정하고 코딩하라. 그리고 주석으로 그 이유를 서술하라.
         * 	어떤 데이터를 우선적으로 Eviction 시킬 것인지에 대한 정책을 정의하고 구현하여야 한다.
         * 	코드에 주석으로 왜 그런 정책을 사용했는지 서술하라.
         * 	(용어)
         * 	Eviction = LRU, MRU, FIFO, LIFO 등의 우선 순위로  구현 기법 (자유 구현)
         *
         * 캐시에서 객체를 조회하는 시점 'get(String key)' 에서도 단일 객체에 대한 만료 처리를 진행하지만
         * 스위퍼 쓰레드를 통해 만료되었으나 더 이상 hit 되지 않는 객체를 제거한다.
         *
         */
        executorService.scheduleAtFixedRate(
                ()->{
                    sweep();
                },
                2,
                10,
                TimeUnit.SECONDS
        );
    }
~~~

~~~java
    /**
     * case 3. Cache Data Eviction Policy
     * 캐시에 적재된 데이터 조회 시점에 ttl 확인하여 만료된 객체는 제거하여 새로운 데이터를 적재할 수 있도록 한다.
     * @param key
     * @return
     */
    Object get(String key){
        CacheObject cache = map.get(key);

        if(cache==null) {
            logger.info("Cache missed!! key '{}'",()->key);
            return null;
        }

        if(cache.expired()){
            map.remove(key);
            logger.info("Cache expired!! key '{}'",()->key);
            return null;
        }

        logger.info("Cache hit!! key '{}'",()->key);
        return cache.element();
    }
~~~

#### case 4.  
~~~java
    /**
     * 1차 카테고리 목록
     * 
     * case 4. Cache Optimization (Cache Miss의 처리 및 최소화 방법)
     * 	Cache Miss를 최소화하기 위한 비즈니스 로직이나 알고리즘을 제안하고 코드로 구현하라.
     *
     * ttl을 길게 주는것이 캐시 효율을 높이는 방안 중 하나이므로
     * 각 데이터의 갱신 주기를 고려하여 ttl을 설정한다.
     * 
     * @return
     */
    @Cache(ttl = 30000L)
    public List<Category> categoryList(){
        return dataProvidor.categoryList();
    }
~~~


#### case 5.  
~~~java
/**
 * case 5. 원본 데이터에 대한 ADD / DELETE / UPDATE
 * 	Note! Cache 자체의 기능은 아니며, 문제 풀이의 구현상 편의를 위하여 클라이언트가 데이터베이스의 원본을 직접 변경하는 행위를 모사하는 API를 Cache Service에서 제공한다.
 * 	(원래 현실에서는 별도의 API를 통하여 일어나는 행위이므로 Cache Service 입장에서는 실시간으로 인지할 수 없는 transaction임)
 * 	특정 카테고리명을 변경할 수 있어야 한다.
 * 	특정 상품명을 변경할 수 있어야 한다.
 * 	특정 상품의 가격을 변경할 수 있어야 한다.
 * 	위의 동작들에 대하여 Cache가 아닌 원본 데이터베이스의 내용을 직접 변경하여야 한다
 *  
 * @author shinys
 */
@Service
@RequiredArgsConstructor
public class ManageUsecase {
~~~


## Prerequisite
* Java 8 이상
* Gradle 6.4.1
* SpringBoot 2.3.2
* H2



## 소스 얻기
`$ git clone https://bitbucket.org/shinys/in-memory-cache.git`

## Developer Settings
소스를 포함한 모든 text file 인코딩은 UTF-8으로 설정.
> 예외로 .properties 파일은 IOS-8859-1 임.

![IntelliJ Setting](./document/intellij-file-encoding.png)


## Build

`$ gradle clean bootJar --debug`  


## Execute 

`$ java -jar  .\build\libs\in-memory-cache-0.0.1-SNAPSHOT.jar`  

## Test case

캐시와 유스케이스, 레파지토리에 대한 기본적인 테스트는 TestCase 통해 확인 가능. 

## API 일람

> [POSTMAN 파일 다운로드](./document/api-test.postman_collection.json)
> POSTMAN에 임포트하여 API를 호츨할 수 있음.



#### 조회계열 (캐시 적용)

#### #1. 1차 카테고리 조회   
GET http://localhost:8080/api/v1.0/categories   

||req|res|
|---|---|---|
|HTTP Method|GET||
|header|accept : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|| 하단 `응답` 참조|

`응답`  
~~~json
{
    "content": [
        {
            "categoryNo": 1,
            "categoryName": "스킨케어",
            "depth": 1
        },
        {
            "categoryNo": 2,
            "categoryName": "메이크업",
            "depth": 1
        },
        ...
    ],
    "result": "0000",
    "message": "OK"
}
~~~

#### #2. 하위 카테고리 조회   
GET http://localhost:8080/api/v1.0/category/{category_no}/sub-categories   

||req|res|
|---|---|---|
|HTTP Method|GET||
|header|accept : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|| 하단 `응답` 참조|

`응답`  
~~~json
{
    "content": [
        {
            "categoryNo": 7,
            "categoryName": "크랜징폼",
            "parentNo": 1,
            "depth": 2
        },
        {
            "categoryNo": 8,
            "categoryName": "클랜징 티슈",
            "parentNo": 1,
            "depth": 2
        }
    ],
    "result": "0000",
    "message": "OK"
}
~~~

#### #3. 상품 목록 조회   
GET http://localhost:8080/api/v1.0/products?categoryNo=&page=&size=   

||req|res|
|---|---|---|
|HTTP Method|GET||
|header|accept : application/json|content-type : application/json|  
|query string|categoryNo=&page=&size=|n/a|
|body|| 하단 `응답` 참조|

`응답`  
~~~json
{
    "content": [
        {
            "productNo": 1,
            "brandName": "바이탈뷰티(시판)",
            "productName": "바이탈뷰티(아) 슬리머에스 35EA (16)",
            "productPrice": 112500000.00,
            "categoryNo": 1
        },
        {
            "productNo": 2,
            "brandName": "바이탈뷰티(시판)",
            "productName": "바이탈뷰티(아) 극진환 81G.",
            "productPrice": 80000000.00,
            "categoryNo": 1
        },
        ...
    ],
    "result": "0000",
    "message": "OK"
}
~~~

#### #4. 상품 상세 조회   
GET http://localhost:8080/api/v1.0/product/{productNo}   

||req|res|
|---|---|---|
|HTTP Method|GET||
|header|accept : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|| 하단 `응답` 참조|

`응답`  
~~~json
{
    "content": {
        "productNo": 1,
        "brandName": "바이탈뷰티(시판)",
        "productName": "바이탈뷰티(아) 슬리머에스 35EA (16)",
        "productPrice": 112500000.00,
        "categoryNo": 1
    },
    "result": "0000",
    "message": "OK"
}
~~~

#### 수정계열  
#### #5. 카테고리 등록  
POST http://localhost:8080/api/v1.0/category   

||req|res|
|---|---|---|
|HTTP Method|POST||
|header|content-type : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|하단 `요청바디` 참조| 하단 `응답` 참조|

`요청바디`  
~~~json
{
    "categoryName": "크랜징폼11",
    "parentNo": 1,
    "depth": 2
}
~~~

`응답`  
~~~json
{
    "content": {
        "categoryNo": 11,
        "categoryName": "크랜징폼11",
        "parentNo": 1,
        "depth": 2
    },
    "result": "0000",
    "message": "OK"
}
~~~



#### #6. 카테고리 명 수정   
PATCH http://localhost:8080/api/v1.0/category/{categoryNo}   

||req|res|
|---|---|---|
|HTTP Method|PATCH||
|header|content-type : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|하단 `요청바디` 참조| 하단 `응답` 참조|

`요청바디`  
~~~json
{
    "categoryName": "크랜징폼22"
}
~~~

`응답`  
~~~json
{
    "content": {
        "categoryNo": 11,
        "categoryName": "크랜징폼22",
        "parentNo": 1,
        "depth": 2
    },
    "result": "0000",
    "message": "OK"
}
~~~

#### #7. 카테고리 삭제 
DELETE http://localhost:8080/api/v1.0/category/{categoryNo} 

`응답`  
~~~json
{
    "result": "0000",
    "message": "OK"
}
~~~

#### #8. 상품 등록  
POST http://localhost:8080/api/v1.0/product   

||req|res|
|---|---|---|
|HTTP Method|POST||
|header|content-type : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|하단 `요청바디` 참조| 하단 `응답` 참조|

`요청바디`  
~~~json
{
    "brandName": "바이탈뷰티(시판1)",
    "productName": "바이탈뷰티(아1) 슬리머에스 35EA (17)",
    "productPrice": 112500000.00,
    "categoryNo": 1
}
~~~

`응답`  
~~~json
{
    "content": {
        "productNo": 1001,
        "brandName": "바이탈뷰티(시판1)",
        "productName": "바이탈뷰티(아1) 슬리머에스 35EA (17)",
        "productPrice": 112500000.00,
        "categoryNo": 1
    },
    "result": "0000",
    "message": "OK"
}
~~~

#### #9. 상품 수정  
PATCH http://localhost:8080/api/v1.0/product/{productNo}   

||req|res|
|---|---|---|
|HTTP Method|PATCH||
|header|content-type : application/json|content-type : application/json|  
|query string|n/a|n/a|
|body|하단 `요청바디` 참조| 하단 `응답` 참조|

`요청바디`  
~~~json
//상품명, 가격 수정
{
    "productName": "바이탈뷰티(아1) 슬리머에스 35EA (17)",
    "productPrice": 222500000.00
}

//가격 수정
{
    "productPrice": 222500000.00
}

//상품명 수정
{
    "productName": "바이탈뷰티(아1) 슬리머에스 35EA (17)"
}

~~~

`응답`  
~~~json
{
    "content": {
        "productNo": 1001,
        "brandName": "바이탈뷰티(시판1)",
        "productName": "바이탈뷰티(아1) 슬리머에스 35EA (17)",
        "productPrice": 222500000.00,
        "categoryNo": 1
    },
    "result": "0000",
    "message": "OK"
}
~~~


#### #10. 상품 삭제   
DELETE http://localhost:8080/api/v1.0/product/{productNo} 

`응답`  
~~~json
{
    "result": "0000",
    "message": "OK"
}
~~~

## 어플리케이션 구성  
> 어플리케이션 구성의 컨셉은 Clean Architecture에 기반하고 Hexagonal 개념으로 구조를 잡음.   
> 어플리케이션의 중심은 usecase(요구기능,비지니스).  

| 이 어플리케이션 구현 방식  |  flow  |
|---|---|
|![Hexagonal Architecture](./document/the-clean-architecture-hexagonal.png) |![](./document/CleanArchitectureCommonFlow-1024x884.jpg) |


#### #소스 패키지 구조
```
클래스, 패키지 배치 원칙
-  운영 시 요건 변경에따른 코드 변경이 다른 usecase에 예상치 않은 영향을 미치지 않도록하는 관점으로 기능 배치.

cache/                   캐시 구현체와 AOP지원을 위한 애노테이션, 애스펙트 위치.

commerce/                과제 요구 기능 구현 패키지.
  data/                  repository 처리 패키지. usecase의 IDataProvider 구현체 역시 이 패키지에 존재.
    DataProvider.java
    xxxRepository.java

  usecase/               업무,도메인 usecase 패키지
    IDataProvider.java
    xxxUsecase.java      업무처리 클래스. 캐시는 이곳에 적용.
            
  web/                   HTTP API 패키지.
    xxxEntrypoint.java   Rest API 진입 지점.   

```


## REST API 작성
* Content-Type은 application/json
* @PostMapping() , @GetMapping()보다는 @RequestMapping() 어노테이션을 사용한다.
`
API(https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/PostMapping.html)
에서 명시하듯 @RequestMapping(method = RequestMethod.POST) 의 shortcut 그 이상도 이하도 아님.
경험상 IDE 나 text 검색 도구를 통한 전체 API URI 검색이 불편함.
`
* path는 소문자를 사용한다.
* URI 마지막에 `/` 를 포함하지 않는다.
`
http://localhost:8080/api/v1.0/category
`
* 언더바(_)가아닌 대시(-)를 사용한다.
`
http://localhost:8080/api/v1.0/category/1/sub-categories 
`
* 컨트롤 자원을 의미하는 URL에는 예외적으로 동사를 허용한다.
`
http://localhost:8080/api/v1.0/product
`


* Method는 GET, POST, PUT, DELETE 허용 이기에 verbose를 path에 기술할 필요는 없으나 모니터링도구, 로그운영 식별의 편의를 고려하여 path에 Update, Delete 에관한 힌트 허용.    
~~~
POST http://localhost:8080/api/v1.0/create/product 
GET http://localhost:8080/api/v1.0/read/product  
GET http://localhost:8080/api/v1.0/list/product?sort=  
~~~


* URI에 불필요한 path를 추가하지 말자 - 3 depth 이내. /{value1}/{value2}/{value3} 형식은 지양. /key1/{value1}/key2/{value2}/key3/{value3}  
~~~
다양한 조건을 path에 넣을 필요는 없다. 유일 자원 식별이 아나리면 쿼리스트링을 활용하자.
GET http://localhost:8080/api/v1.0/product?page=&size=
~~~

