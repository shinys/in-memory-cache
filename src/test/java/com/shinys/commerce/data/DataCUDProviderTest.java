package com.shinys.commerce.data;


import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.usecase.IDataCUDProvider;
import com.shinys.commerce.usecase.IDataReadProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Random;

@SpringBootTest
public class DataCUDProviderTest {
    @Autowired
    IDataCUDProvider dataCUDProvider;

    @Autowired
    IDataReadProvider dataReadProvider;

    @DisplayName("카테고리 생성 키 중복")
    @Test
    public void createFailure(){
        int beforeSize = dataReadProvider.categoryList().size();

        Category category = new Category();
        category.setCategoryNo(1);
        category.setCategoryName("테스트카테고리");
        category.setDepth(1);
        category.setParentNo(null);

        dataCUDProvider.createCategory(category);

        int afterSize = dataReadProvider.categoryList().size();

        Assertions.assertEquals(beforeSize,afterSize);

    }

    @DisplayName("카테고리 생성 키 중복")
    @Test
    public void createSuccess(){
        int beforeSize = dataReadProvider.categoryList().size();

        Category category = new Category();
        category.setCategoryNo(100);
        category.setCategoryName("테스트카테고리");
        category.setDepth(1);
        category.setParentNo(null);

        dataCUDProvider.createCategory(category);

        int afterSize = dataReadProvider.categoryList().size();

        Assertions.assertEquals(beforeSize+1,afterSize);

    }

    @DisplayName("카테고리 이름 변경")
    @Test
    public void renameCategory(){

        Category saved = dataReadProvider.categoryList().get(new Random().nextInt(5));

        Integer savedCategoryNo = saved.getCategoryNo();
        String savedCategoryName = saved.getCategoryName();


        dataCUDProvider.updateCategoryName(savedCategoryNo, "바꾼카테고리명");


        Category after = dataReadProvider.categoryList().stream().filter(c->{return c.getCategoryNo()==savedCategoryNo;}).findFirst().orElse(new Category());

        Assertions.assertEquals("바꾼카테고리명", after.getCategoryName());

    }



    @DisplayName("상품 이름 변경")
    @Test
    public void renameProduct(){

        Product saved = dataReadProvider.product(Long.valueOf(new Random().nextInt(400)));

        Long savedProductNo = saved.getProductNo();
        String savedProductName = saved.getProductName();
        BigDecimal savedPrice = saved.getProductPrice();


        dataCUDProvider.updateProduct(savedProductNo, "바꾼제품명",null);


        Product after1 = dataReadProvider.product(savedProductNo);
        Assertions.assertNotEquals("바꾼제품명", savedProductName);
        Assertions.assertEquals(savedPrice, after1.getProductPrice());
        Assertions.assertEquals("바꾼제품명",after1.getProductName());

        dataCUDProvider.updateProduct(savedProductNo, null,new BigDecimal(233333));

        Product after2 = dataReadProvider.product(savedProductNo);
        Assertions.assertEquals(new BigDecimal(233333).setScale(2), after2.getProductPrice());
        Assertions.assertEquals("바꾼제품명",after2.getProductName());

    }
}
