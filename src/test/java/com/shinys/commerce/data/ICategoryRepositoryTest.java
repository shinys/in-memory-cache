package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.exceptions.CategoryNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Random;

@DataJpaTest
@Transactional
public class ICategoryRepositoryTest {

    @Autowired
    ICategoryRepository categoryRepository;

    @Autowired
    EntityManager em;

    @DisplayName("초기 1차 카테고리")
    @Test
    public void firstDepth(){
        Assertions.assertEquals(6, categoryRepository.findByDepth(1).size());
    }

    @DisplayName("초기 2차 카테고리")
    @Test
    public void secondDepth(){
        Assertions.assertEquals(4, categoryRepository.findByDepth(2).size());
    }

    @DisplayName("카테고리 수정")
    @Test
    public void updateCategory(){
        final int no = new Random().nextInt(9);

        Category before = categoryRepository.findById(no).orElseThrow(()->new CategoryNotFoundException(no));
        String beforeName = before.getCategoryName();
        before.setCategoryName(before.getCategoryName().concat( " 수정"));

        Category after = categoryRepository.findById(no).orElseThrow(()->new CategoryNotFoundException(no));

        Assertions.assertEquals(beforeName.concat( " 수정"), after.getCategoryName());
    }

}
