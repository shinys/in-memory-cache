package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

@DataJpaTest
@Transactional
public class IProductRepositoryTest {

    @Autowired
    IProductRepository productRepository;

    @Autowired
    EntityManager em;

    Product product;

    @BeforeEach
    void before(){
        Product product = new Product();
        product.setProductNo(UniqueId.nextProductNo());
        product.setCategoryNo(1);
        product.setProductPrice(new BigDecimal(100));
        product.setProductName("제품명");
        product.setBrandName("브랜드명");
        this.product = product;
    }

    @DisplayName("제품 저장")
    @Test
    public void create(){
        Product saved = productRepository.save(product);
        Assertions.assertEquals(this.product.getProductNo(),saved.getProductNo());
    }

    @DisplayName("제품 조회")
    @Test
    public void findList(){
        Product saved = productRepository.save(product);
        Assertions.assertEquals(this.product.getProductNo(),saved.getProductNo());
        List<Product> productList = productRepository.findByCategoryNo(1, PageRequest.of(0,200)).toList();
        System.out.println(productList);
        Assertions.assertEquals(101, productList.size());
    }


    @DisplayName("제품 조회")
    @Test
    public void findOne(){
        Product product = productRepository.findById(1L).orElse(new Product());
        Assertions.assertEquals(1L, product.getProductNo());
    }


    @DisplayName("제품 카테고리 일괄 업데이트")
    @Test
    public void updateCategoryNo(){
        productRepository.updateCategoryNo(1,0);
        Page<Product> products = productRepository.findByCategoryNo(1,PageRequest.of(0,100));
        Assertions.assertTrue(products.isEmpty());
    }
}
