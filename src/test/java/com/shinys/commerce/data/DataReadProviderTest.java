package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.usecase.IDataReadProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DataReadProviderTest {
    @Autowired
    IDataReadProvider dataReadProvider;

    @DisplayName("초기 카테고리 데이터 로드 확인")
    @Test
    public void categoryExists(){
        Assertions.assertEquals( 6, dataReadProvider.categoryList().size()  );
    }

    @DisplayName("초기 제품 데이터 로드 확인")
    @Test
    public void productExists(){
        Assertions.assertEquals( "바이탈뷰티(아) 슬리머에스 35EA (16)", dataReadProvider.product(1L).getProductName()  );
    }

    @DisplayName("상품 정보 캐시 확인")
    @Test
    public void cachedProduct(){

        Product product1 = dataReadProvider.product(2L);

        Product product2 = dataReadProvider.product(2L);

        Assertions.assertEquals(product1,product2);

        Product product3 = dataReadProvider.product(2L);
        Assertions.assertEquals(product2,product3);

    }

}
