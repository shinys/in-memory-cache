package com.shinys.cache;



import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class CacheManagerTest {

    private CacheManager cacheManager;

    @BeforeEach
    public void before(){
        cacheManager = new CacheManager();
    }

    @DisplayName("객체 캐싱")
    @Test
    public void caching(){

        ArrayList str1 = new ArrayList();
        ArrayList str2 = new ArrayList();

        Assertions.assertFalse(str1==str2);

        cacheManager.put("saved",str1,3000L);

        Assertions.assertTrue(str1==cacheManager.get("saved"));

    }

    @DisplayName("캐시 제거")
    @Test
    public void remove(){
        ArrayList str1 = new ArrayList();
        ArrayList str2 = new ArrayList();

        Assertions.assertFalse(str1==str2);

        cacheManager.put("saved",str1,3000L);

        Assertions.assertTrue(str1==cacheManager.get("saved"));

        cacheManager.remove("saved");

        Assertions.assertTrue(cacheManager.get("saved")==null);

    }

    @DisplayName("캐시 TTL 확인")
    @Test
    public void ttl() throws InterruptedException {

        ArrayList str1 = new ArrayList();
        ArrayList str2 = new ArrayList();

        Assertions.assertFalse(str1==str2);

        cacheManager.put("saved",str1,13000L);

        Assertions.assertTrue(str1==cacheManager.get("saved"));

        Thread.sleep(13005L);

        Assertions.assertTrue(cacheManager.get("saved")==null);

    }
}
