package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * @author shinys
 */
@RepositoryRestResource(collectionResourceRel = "category", path = "category")
public interface ICategoryRepository extends JpaRepository<Category, Integer> {

    List<Category> findByDepth(int depth);

    List<Category> findByParentNo(Integer parentNo);

    @Query(value = "UPDATE Category c SET c.categoryName = :categoryName WHERE c.categoryNo = :categoryNo")
    void updateCategoryName(Integer categoryNo, String categoryName);
}
