package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.data.exceptions.CategoryNotFoundException;
import com.shinys.commerce.data.exceptions.ProductNotFoundException;
import com.shinys.commerce.usecase.IDataCUDProvider;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @author shinys
 */
@Component
@RequiredArgsConstructor
public class DataCUDProvider implements IDataCUDProvider {
    private static final Logger logger = LogManager.getLogger(DataCUDProvider.class);

    private final ICategoryRepository categoryRepository;

    private final IProductRepository productRepository;

    @Transactional(rollbackFor=Exception.class)
    @Override
    public Category createCategory(final Category category) {
        if(category.getParentNo()>0){
            categoryRepository.findById(category.getParentNo())
                    .orElseThrow(()->new CategoryNotFoundException(category.getParentNo()));
        }
        category.setCategoryNo(UniqueId.nextCategoryNo());
        return categoryRepository.save(category);
    }

    @Transactional(rollbackFor=Exception.class)
    @Modifying
    @Override
    public Category updateCategoryName(final Integer categoryNo, String categoryName) {
        Category category = categoryRepository.findById(categoryNo)
                .orElseThrow(()->new CategoryNotFoundException(categoryNo));
        category.setCategoryName(categoryName);
        return category;
    }

    @Transactional(rollbackFor=Exception.class)
    @Modifying(clearAutomatically = true)
    @Override
    public void deleteCategory(final Integer categoryNo) {
        //삭제 카테고리의 상품들의 카테고리를 미할당(0)으로 업데이트
        productRepository.updateCategoryNo(categoryNo, 0);
        categoryRepository.deleteById(categoryNo);
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public Product createProduct(final Product product) {
        categoryRepository.findById(product.getCategoryNo())
        .orElseThrow(()->new CategoryNotFoundException(product.getCategoryNo()));

        product.setProductNo(UniqueId.nextProductNo());
        return productRepository.save(product);
    }

    @Transactional(rollbackFor=Exception.class)
    @Modifying
    @Override
    public Product updateProduct(final Long productNo, final String productName, final BigDecimal productPrice) {
        Product product = productRepository.findById(productNo)
                .orElseThrow(()-> new ProductNotFoundException(productNo));
        if(productName!=null)
            product.setProductName(productName);
        if(productPrice!=null)
            product.setProductPrice(productPrice);
        ;
        return product;
    }

    @Transactional(rollbackFor=Exception.class)
    @Modifying
    @Override
    public void deleteProduct(final Long productNo) {
        productRepository.deleteById(productNo);
    }
}
