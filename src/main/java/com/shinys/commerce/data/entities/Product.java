package com.shinys.commerce.data.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 *
 * @author shinys
 */
@Data
@Entity
@ToString
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p"),
    @NamedQuery(name = "Product.findByProductNo", query = "SELECT p FROM Product p WHERE p.productNo = :productNo"),
    @NamedQuery(name = "Product.findByBrandName", query = "SELECT p FROM Product p WHERE p.brandName = :brandName"),
    @NamedQuery(name = "Product.findByProductName", query = "SELECT p FROM Product p WHERE p.productName = :productName"),
    @NamedQuery(name = "Product.findByProductPrice", query = "SELECT p FROM Product p WHERE p.productPrice = :productPrice"),
    @NamedQuery(name = "Product.findByCategoryNo", query = "SELECT p FROM Product p WHERE p.categoryNo = :categoryNo")})
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "product_no")
    private Long productNo;
    @Column(name = "brand_name")
    private String brandName;
    @Column(name = "product_name")
    private String productName;
    @Min(value = 0) @Max(value = 100000000)
    @Column(name = "product_price")
    private BigDecimal productPrice;
    @Column(name = "category_no")
    private Integer categoryNo;

    public Product() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productNo != null ? productNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productNo == null && other.productNo != null) || (this.productNo != null && !this.productNo.equals(other.productNo))) {
            return false;
        }
        return true;
    }

}
