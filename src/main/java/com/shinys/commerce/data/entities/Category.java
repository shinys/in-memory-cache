package com.shinys.commerce.data.entities;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 *
 * @author shinys
 */
@Data
@Entity
@ToString
@NamedQueries({
    @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c"),
    @NamedQuery(name = "Category.findByCategoryNo", query = "SELECT c FROM Category c WHERE c.categoryNo = :categoryNo"),
    @NamedQuery(name = "Category.findByCategoryName", query = "SELECT c FROM Category c WHERE c.categoryName = :categoryName"),
    @NamedQuery(name = "Category.findByParentNo", query = "SELECT c FROM Category c WHERE c.parentNo = :parentNo"),
    @NamedQuery(name = "Category.findByDepth", query = "SELECT c FROM Category c WHERE c.depth = :depth")})
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "category_no")
    private Integer categoryNo;
    @NotNull
    @Column(name = "category_name")
    private String categoryName;
    @Column(name = "parent_no")
    private Integer parentNo;
    @Basic(optional = false)
    private int depth;

    public Category() {
    }

    public Category(Integer categoryNo) {
        this.categoryNo = categoryNo;
    }

    public Category(Integer categoryNo, int depth) {
        this.categoryNo = categoryNo;
        this.depth = depth;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryNo != null ? categoryNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.categoryNo == null && other.categoryNo != null) || (this.categoryNo != null && !this.categoryNo.equals(other.categoryNo))) {
            return false;
        }
        return true;
    }

    
}
