package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shinys
 */
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "product", path = "product")
public interface IProductRepository extends JpaRepository<Product, Long> {

    Page<Product> findByCategoryNo(Integer categoryNo, Pageable pageable);

    Optional<Product> findById(Long productNo);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Product p SET p.categoryNo = :updateNo WHERE p.categoryNo = :categoryNo")
    void updateCategoryNo(@Param(value = "categoryNo") Integer categoryNo, @Param(value = "updateNo") Integer updateNo);

}
