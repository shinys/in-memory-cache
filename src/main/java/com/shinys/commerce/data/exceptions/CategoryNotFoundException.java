package com.shinys.commerce.data.exceptions;

import com.shinys.commerce.web.ResultCode;

public class CategoryNotFoundException extends RuntimeException {
    private ResultCode code = ResultCode.DATA_NOT_FOUND;
    private Integer categoryNo;

    private CategoryNotFoundException(){
        super();
    }

    public CategoryNotFoundException(Integer categoryNo){
        this(categoryNo,String.format("categoryNo %d not exists.",categoryNo));
    }

    public CategoryNotFoundException(Integer categoryNo, String mgs){
        super(mgs);
        this.categoryNo = categoryNo;
    }

    public Integer getCategoryNo(){
        return this.categoryNo;
    }

    public ResultCode getCode(){
        return code;
    }


}
