package com.shinys.commerce.data.exceptions;

import com.shinys.commerce.web.ResultCode;

public class ProductNotFoundException extends RuntimeException {
    private ResultCode code = ResultCode.DATA_NOT_FOUND;
    private Long productNo;

    private ProductNotFoundException(){
        super();
    }

    public ProductNotFoundException(Long productNo){
        this(productNo,String.format("productNo %d not exists.",productNo));
    }

    public ProductNotFoundException(Long productNo, String mgs){
        super(mgs);
        this.productNo = productNo;
    }

    public Long getProductNo(){
        return this.productNo;
    }

    public ResultCode getCode(){
        return code;
    }


}
