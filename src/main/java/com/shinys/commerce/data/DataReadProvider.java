package com.shinys.commerce.data;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.data.exceptions.ProductNotFoundException;
import com.shinys.commerce.usecase.IDataReadProvider;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author shinys
 */
@Component
@RequiredArgsConstructor
public class DataReadProvider implements IDataReadProvider {
    private static final Logger logger = LogManager.getLogger(DataReadProvider.class);

    private final ICategoryRepository categoryRepository;

    private final IProductRepository productRepository;


    @Override
    public List<Category> categoryList() {
        return categoryRepository.findByDepth(1);
    }

    @Override
    public List<Category> subCategoryList(final Integer parentCategoryNo) {
        return categoryRepository.findByParentNo(parentCategoryNo);
    }

    @Override
    public List<Product> productList(final Integer categoryNo, final int page, final int size) {
        return productRepository.findByCategoryNo(categoryNo,PageRequest.of(page-1, size)).toList();
    }

    @Override
    public Product product(final Long productNo) {
        return productRepository.findById(productNo).orElseThrow(()->new ProductNotFoundException(productNo));
    }
}
