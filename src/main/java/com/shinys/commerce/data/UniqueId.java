package com.shinys.commerce.data;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author shinys
 */
public class UniqueId {

    private static final AtomicInteger categoryNo = new AtomicInteger(10);
    private static final AtomicLong productNo = new AtomicLong(1000);

    public static Integer nextCategoryNo(){
        return categoryNo.incrementAndGet();
    }

    public static Long nextProductNo(){
        return productNo.incrementAndGet();
    }
}
