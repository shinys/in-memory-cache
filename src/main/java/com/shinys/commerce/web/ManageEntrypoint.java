package com.shinys.commerce.web;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.usecase.ManageUsecase;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * case 5. 원본 데이터에 대한 ADD / DELETE / UPDATE
 * 	Note! Cache 자체의 기능은 아니며, 문제 풀이의 구현상 편의를 위하여 클라이언트가 데이터베이스의 원본을 직접 변경하는 행위를 모사하는 API를 Cache Service에서 제공한다.
 * 	(원래 현실에서는 별도의 API를 통하여 일어나는 행위이므로 Cache Service 입장에서는 실시간으로 인지할 수 없는 transaction임)
 * 	특정 카테고리명을 변경할 수 있어야 한다.
 * 	특정 상품명을 변경할 수 있어야 한다.
 * 	특정 상품의 가격을 변경할 수 있어야 한다.
 * 	위의 동작들에 대하여 Cache가 아닌 원본 데이터베이스의 내용을 직접 변경하여야 한다
 * @author shinys
 */
@RestController
@RequestMapping(value = "/v1.0")
@RequiredArgsConstructor
public class ManageEntrypoint {
    private static final Logger logger = LogManager.getLogger(ManageEntrypoint.class);

    private final ManageUsecase manageUsecase;

    /**
     * 카테고리 생성
     * @param category
     * @return
     */
    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public ResponseDTO<Category> createCategory(
            @RequestBody Category category
    ){
        return ResponseDTO.ok(manageUsecase.createCategory(category));
    }

    /**
     * 카테고리명 수정
     * @param categoryNo
     * @param category
     * @return
     */
    @RequestMapping(value = "/category/{categoryNo}", method = RequestMethod.PATCH)
    public ResponseDTO<Category> updateCategory(
            @PathVariable(value = "categoryNo") Integer categoryNo,
            @RequestBody @Valid Category category
    ){
        return ResponseDTO.ok(manageUsecase.updateCategoryName(
                categoryNo,category.getCategoryName()
        ));
    }

    /**
     * 카테고리 삭제
     * @param categoryNo
     * @return
     */
    @RequestMapping(value = "/category/{categoryNo}", method = RequestMethod.DELETE)
    public ResponseDTO<Void> deleteCategory(
            @PathVariable(value = "categoryNo") Integer categoryNo
    ){
        manageUsecase.deleteCategory(categoryNo);
        return ResponseDTO.ok(null);
    }


    /**
     * 상품 등록
     * @param product
     * @return
     */
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public ResponseDTO<Product> createProduct(
            @RequestBody @Valid Product product
    ){
        return ResponseDTO.ok(manageUsecase.createProduct(product));
    }

    /**
     * 상품 수정. 상품명, 가격
     * @param productNo
     * @param product
     * @return
     */
    @RequestMapping(value = "/product/{productNo}", method = RequestMethod.PATCH)
    public ResponseDTO<Product> updateProduct(
            @PathVariable(value = "productNo") Long productNo,
            @RequestBody Product product
    ){
        return ResponseDTO.ok(manageUsecase.updateProduct(
                productNo,product.getProductName(),product.getProductPrice()
        ));
    }

    /**
     * 상품 삭제
     * @param productNo
     * @return
     */
    @RequestMapping(value = "/product/{productNo}", method = RequestMethod.DELETE)
    public ResponseDTO<Product> deleteProduct(
            @PathVariable(value = "productNo") Long productNo
    ){
        manageUsecase.deleteProduct(productNo);
        return ResponseDTO.ok(null);
    }
}
