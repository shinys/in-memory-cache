package com.shinys.commerce.web;

import lombok.Getter;
import org.springframework.context.annotation.Scope;

@Scope("request")
public enum ResultCode {

    BAD_REQUEST("400", "Bad Request"),
    RESOURCE_NOT_FOUND("404", "Resource Not Found"),
    TOO_MANY_REQUESTS("429","TOO MANY REQUESTS"),

    RUNTIME_EXCEPTION("500", "Internal Server Error"),
    GATEWAY_TIMEOUT("504","Gateway Timeout"),

    //// 어플리케이션 응답 코드
    OK("0000", "OK"),
    DATA_NOT_FOUND("2404", "Not Found Data"),
    DATA_ALREADY_EXISTS("2409","Data Already Exists"),

    ;

    @Getter
    public String code;

    @Getter
    public String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
