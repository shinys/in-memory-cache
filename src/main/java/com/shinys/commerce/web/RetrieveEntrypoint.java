package com.shinys.commerce.web;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import com.shinys.commerce.usecase.RetrieveUsecase;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author shinys
 */
@RestController
@RequestMapping(value = "/v1.0")
@RequiredArgsConstructor
public class RetrieveEntrypoint {
    private static final Logger logger = LogManager.getLogger(RetrieveEntrypoint.class);

    private final RetrieveUsecase retrieveUsecase;

    /**
     * 1차 카테고리 목록 조회
     * @return
     */
    @RequestMapping(value = "/categories" , method = RequestMethod.GET)
    public ResponseDTO<List<Category>> categories(){
        return ResponseDTO.ok( retrieveUsecase.categoryList() );
    }

    /**
     * 2차 카테고리 목록 조회
     * @param categoryNo
     * @return
     */
    @RequestMapping(value = "/category/{categoryNo}/sub-categories" , method = RequestMethod.GET)
    public ResponseDTO<List<Category>> categories(
            @PathVariable(value = "categoryNo", required = true) Integer categoryNo
    ){
        return ResponseDTO.ok( retrieveUsecase.subCategoryList(categoryNo) );
    }

    /**
     * 특정 카테고리의 상품목록
     * @param categoryNo
     * @param size
     * @param page
     * @return
     */
    @RequestMapping(value = "/products" , method = RequestMethod.GET)
    public ResponseDTO<List<Product>> products(
            @RequestParam(value = "categoryNo", required = true) Integer categoryNo,
            @RequestParam(value = "size", required = true, defaultValue = "20") int size,
            @RequestParam(value = "page", required = true, defaultValue = "1") int page
    ){
        return ResponseDTO.ok( retrieveUsecase.productList(categoryNo,page,size) );
    }

    /**
     * 특정 상품 상세
     * @param productNo
     * @return
     */
    @RequestMapping(value = "/product/{productNo}" , method = RequestMethod.GET)
    public ResponseDTO<Product> product(
            @PathVariable(value = "productNo") Long productNo
    ){
        logger.info("입력값:{}",()->productNo);
        return ResponseDTO.ok( retrieveUsecase.product(productNo) );
    }
}
