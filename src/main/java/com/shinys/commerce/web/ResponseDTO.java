package com.shinys.commerce.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ResponseDTO<T> {
    private T content;
    private String result;
    private String message;
    private StackTraceElement[] trace;

    @JsonIgnore
    public static <T> ResponseDTO<T> ok(T data){
        ResponseDTO res = new ResponseDTO();
        res.result = ResultCode.OK.code;
        res.message = ResultCode.OK.message;
        res.content = data;
        return res;
    }

    @JsonIgnore
    public static <T> ResponseDTO<T> error(T data, ResultCode resultCode){
        ResponseDTO res = new ResponseDTO();
        res.result = resultCode.code;
        res.message = resultCode.message;
        res.content = data;
        return res;
    }


    @JsonIgnore
    public static ResponseDTO<StackTraceElement[]> error(StackTraceElement[] trace, ResultCode resultCode, String message){
        ResponseDTO res = new ResponseDTO();
        res.result = resultCode.code;
        res.message = message;
        res.trace = trace;
        return res;
    }
}
