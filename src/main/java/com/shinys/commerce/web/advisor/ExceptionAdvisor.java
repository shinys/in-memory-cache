package com.shinys.commerce.web.advisor;

import com.shinys.commerce.data.exceptions.CategoryNotFoundException;
import com.shinys.commerce.data.exceptions.ProductNotFoundException;
import com.shinys.commerce.web.ResponseDTO;
import com.shinys.commerce.web.ResultCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.transaction.SystemException;

/**
 * @author shinys
 */
@ControllerAdvice
@RestController
public class ExceptionAdvisor {
    Logger logger = LogManager.getLogger("RuntimeException");

    @Order(Ordered.LOWEST_PRECEDENCE-300)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseDTO<String> methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        logger.error(exception.getCause(), exception);
        BindingResult bindingResult = exception.getBindingResult();

        StringBuilder builder = new StringBuilder();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            builder.append("['");
            builder.append(fieldError.getField());
            builder.append("' must ");
            builder.append(fieldError.getDefaultMessage());
            builder.append(" input value : '");
            builder.append(fieldError.getRejectedValue());
            builder.append("'] ");
        }

        return  ResponseDTO.error(builder.toString(), ResultCode.BAD_REQUEST);
    }

    @Order(Ordered.LOWEST_PRECEDENCE-220)
    @ExceptionHandler(ProductNotFoundException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseDTO<StackTraceElement[]> productNotFoundException(ProductNotFoundException exception) {
        logger.error(exception.getCause(), exception);
        return  ResponseDTO.error( getStackTrace(exception), exception.getCode(), exception.getMessage());
    }

    @Order(Ordered.LOWEST_PRECEDENCE-219)
    @ExceptionHandler(CategoryNotFoundException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseDTO<StackTraceElement[]> categoryNotFoundException(CategoryNotFoundException exception) {
        logger.error(exception.getCause(), exception);
        return  ResponseDTO.error( getStackTrace(exception), exception.getCode(), exception.getMessage());
    }


    @Order(Ordered.LOWEST_PRECEDENCE-210)
    @ExceptionHandler(SystemException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDTO<StackTraceElement[]> illegalArgumentException(SystemException exception) {
        logger.error(exception.getCause(), exception);
        return  ResponseDTO.error( getStackTrace(exception), ResultCode.RUNTIME_EXCEPTION, exception.toString());
    }


    @Order(Ordered.LOWEST_PRECEDENCE-200)
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ResponseDTO<StackTraceElement[]> noHandlerFoundException(NoHandlerFoundException exception) {
        return  ResponseDTO.error( null, ResultCode.RESOURCE_NOT_FOUND, exception.getHttpMethod() +" "+ exception.getRequestURL());
    }


    @Order(Ordered.LOWEST_PRECEDENCE-100)
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDTO<StackTraceElement[]> nullPointerException(NullPointerException exception) {
        logger.error(exception.getCause(), exception);
        StackTraceElement[] traces = exception.getStackTrace();
        logger.error("{}", exception::getMessage, ()-> exception);
        return  ResponseDTO.error(getStackTrace(exception), ResultCode.RUNTIME_EXCEPTION, exception.getMessage()==null?exception.toString():exception.toString()+" : "+exception.getMessage());
    }


    @Order(Ordered.LOWEST_PRECEDENCE)
    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDTO<StackTraceElement[]> exception(Exception exception) {
        logger.error(exception.getCause(), exception);
        StackTraceElement[] traces = exception.getStackTrace();
        return  ResponseDTO.error(getStackTrace(exception), ResultCode.RUNTIME_EXCEPTION, exception.getMessage()==null?exception.toString():exception.toString()+" : "+exception.getMessage());
    }

    private StackTraceElement[] getStackTrace(Exception exception) {
        StackTraceElement[] traces = exception.getStackTrace();
        StackTraceElement[] report = null;
        if(traces!=null && traces.length >= 3) {
            report = new StackTraceElement[3];
            report[0] = traces[0];
            report[1] = traces[1];
            report[2] = traces[2];
        }else if (traces!=null && traces.length < 3){
            report = new StackTraceElement[1];
            try {
                report[0] = traces[0];
            }catch(Exception e){}
        }
        return  report;
    }
}
