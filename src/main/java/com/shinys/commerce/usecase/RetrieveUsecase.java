package com.shinys.commerce.usecase;


//import com.shinys.commerce.data.DataProvidor;
import com.shinys.cache.Cache;
import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * case 1. Query
 * 	클라이언트 코드는 Cache Service가 제공하는 API를 통하여, 카테고리 리스트를 조회할 수 있어야 한다.
 * 	특정 카테고리에 속한 상품 리스트를 조회할 수 있어야 한다.
 * 	특정 상품에 대하여 상품명, 카테고리, 가격을 조회할 수 있어야 한다.
 *
 * case 2. Data Loading and Reloading
 * 	Cache Miss가 발생하면 적절한 시점에 cache는 스스로 해당 부분의 데이터를 원본 데이터베이스로부터 reloading한다.
 *
 * @Cache 애노테이션처리 부분에서 캐시 miss발생 시 원본 데이터 취득 처리
 *
 * @author shinys
 */
@Service
@RequiredArgsConstructor
public class RetrieveUsecase {
    private static final Logger logger = LogManager.getLogger(RetrieveUsecase.class);

    private final IDataReadProvider dataProvidor;

    /**
     * 1차 카테고리 목록
     *
     * case 4. Cache Optimization (Cache Miss의 처리 및 최소화 방법)
     * 	Cache Miss를 최소화하기 위한 비즈니스 로직이나 알고리즘을 제안하고 코드로 구현하라.
     *
     * ttl을 길게 주는것이 캐시 효율을 높이는 방안 중 하나이므로
     * 각 데이터의 갱신 주기를 고려하여 ttl을 설정한다.
     *
     * @return
     */
    @Cache(ttl = 30000L)
    public List<Category> categoryList(){
        return dataProvidor.categoryList();
    }

    /**
     * 2차 카테고리 목록
     * @param parentCategoryNo
     * @return
     */
    @Cache(ttl = 30000L)
    public List<Category> subCategoryList(Integer parentCategoryNo){
        return dataProvidor.subCategoryList(parentCategoryNo);
    }

    /**
     * 특정 카테고리 상품목록
     * @param categoryNo
     * @param page
     * @param size
     * @return
     */
    @Cache(ttl = 10000L)
    public List<Product> productList(Integer categoryNo, int page, int size){
        return dataProvidor.productList(categoryNo,page,size);
    }

    /**
     * 특정 상품 상세
     * @param productNo
     * @return
     */
    @Cache(ttl = 20000L)
    public Product product(Long productNo){
        return dataProvidor.product(productNo);
    }

}
