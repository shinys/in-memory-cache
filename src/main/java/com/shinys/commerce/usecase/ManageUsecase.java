package com.shinys.commerce.usecase;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * case 5. 원본 데이터에 대한 ADD / DELETE / UPDATE
 * 	Note! Cache 자체의 기능은 아니며, 문제 풀이의 구현상 편의를 위하여 클라이언트가 데이터베이스의 원본을 직접 변경하는 행위를 모사하는 API를 Cache Service에서 제공한다.
 * 	(원래 현실에서는 별도의 API를 통하여 일어나는 행위이므로 Cache Service 입장에서는 실시간으로 인지할 수 없는 transaction임)
 * 	특정 카테고리명을 변경할 수 있어야 한다.
 * 	특정 상품명을 변경할 수 있어야 한다.
 * 	특정 상품의 가격을 변경할 수 있어야 한다.
 * 	위의 동작들에 대하여 Cache가 아닌 원본 데이터베이스의 내용을 직접 변경하여야 한다
 *
 * @author shinys
 */
@Service
@RequiredArgsConstructor
public class ManageUsecase {
    private static final Logger logger = LogManager.getLogger(ManageUsecase.class);

    private final IDataCUDProvider dataCUDProvider;

    /**
     * 카테고리 생성
     * @param category
     * @return
     */
    public Category createCategory(Category category){
        return dataCUDProvider.createCategory(category);
    }

    /**
     * 카테고리 이름 변경
     * @param categoryNo
     * @param categoryName
     */
    public Category updateCategoryName(Integer categoryNo, String categoryName){
        return dataCUDProvider.updateCategoryName(categoryNo,categoryName);
    }

    /**
     * 카테고리 삭제
     * @param categoryNo
     */
    public void deleteCategory(Integer categoryNo){
        dataCUDProvider.deleteCategory(categoryNo);
    }

    /**
     * 상품 등록
     * @param product
     * @return
     */
    public Product createProduct(Product product){
        return dataCUDProvider.createProduct(product);
    }

    /**
     * 상품 명, 가격 수정
     * @param productNo
     * @param productName
     * @param productPrice
     */
    public Product updateProduct(Long productNo, String productName, BigDecimal productPrice){
        return dataCUDProvider.updateProduct(productNo,productName,productPrice);
    }

    /**
     * 상품 삭제
     * @param productNo
     */
    public void deleteProduct(Long productNo){
        dataCUDProvider.deleteProduct(productNo);
    }

}
