package com.shinys.commerce.usecase;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;

import java.util.List;

/**
 * @author shinys
 */
public interface IDataReadProvider {

    List<Category> categoryList();

    List<Category> subCategoryList(Integer parentCategoryNo);

    List<Product> productList(Integer categoryNo, int page, int size);

    Product product(Long productNo);
}
