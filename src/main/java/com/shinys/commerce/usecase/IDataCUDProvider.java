package com.shinys.commerce.usecase;

import com.shinys.commerce.data.entities.Category;
import com.shinys.commerce.data.entities.Product;

import java.math.BigDecimal;

/**
 * @author shinys
 */
public interface IDataCUDProvider {

    Category createCategory(Category category);

    Category updateCategoryName(Integer categoryNo, String categoryName);

    void deleteCategory(Integer categoryNo);

    Product createProduct(Product product);

    Product updateProduct(Long productNo, String productName, BigDecimal productPrice);

    void deleteProduct(Long productNo);
}
