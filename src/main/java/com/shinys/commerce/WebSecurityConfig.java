package com.shinys.commerce;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * @author shinys
 */
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    public UserDetailsService userDetailsService() {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        UserDetails monitor = User.builder()
                .passwordEncoder(encoder::encode)
                .username("shinys")
                .password("shinys1234")
                .roles("MONITOR").build();
        return new InMemoryUserDetailsManager(monitor);
    }

    @Configuration
    @Order(3)
    public static class ApiWebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
//            http.authorizeRequests()
//                    .antMatchers("**")
//                    .permitAll()
//                    .and().csrf()
//                    .and().headers().frameOptions().sameOrigin().xssProtection();

            http.csrf().disable()
                    .headers()
                    // allow same origin to frame our site to support iframe SockJS
                    //SockJS 및 프레임 옵션
                    //SockJS는 iframe을 활용하는 전송을 사용할 수 있습니다 . 기본적으로 Spring Security는 클릭 재킹 공격을 막기 위해 사이트 프레임 이 거부 됩니다 .
                    //SockJS 프레임 기반 전송이 작동하도록하려면 동일한 오리진이 컨텐츠를 프레임화할 수 있도록 Spring Security를 ​​구성해야합니다.
                    //frame-options 요소를 사용하여 X-Frame-Options를 사용자 정의 할 수 있습니다 .
                    //예를 들어, 다음은 Spring Security가 동일한 도메인 내에서 iframe을 허용하는 "X-Frame-Options : SAMEORIGIN"을 사용하도록 지시합니다.
                    .frameOptions().sameOrigin()
                    .and()
                    .authorizeRequests()
                    .anyRequest().permitAll()
            ;
        }
    }

}
