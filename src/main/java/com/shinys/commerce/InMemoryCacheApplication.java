package com.shinys.commerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author shinys
 */
@SpringBootApplication(scanBasePackages = "com.shinys")
@EnableTransactionManagement(proxyTargetClass = true)
public class InMemoryCacheApplication {
	public static void main(String[] args) {
		SpringApplication.run(InMemoryCacheApplication.class, args);
	}
}
