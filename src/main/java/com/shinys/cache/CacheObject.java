package com.shinys.cache;

import java.time.*;
import java.io.Serializable;

/**
 *
 * @param <E>
 * @author shinys
 */
class CacheObject<E> implements Serializable {
    private static final long serialVersionUID = 1L;

    private E obj;
    private long storageTime;
    private long expireTime;

    private CacheObject(){}

    CacheObject(E obj, long ttl){
        this.storageTime =  Instant.now().toEpochMilli();
        this.obj = obj;
        this.expireTime = this.storageTime + ttl;
    }


    boolean expired(){
       return (Instant.now().toEpochMilli() >= this.expireTime);
    }

    E element(){
        return this.obj;
    }

}
