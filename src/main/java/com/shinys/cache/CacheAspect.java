package com.shinys.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @Cache 애너테이션이 적용된 메소드에 캐싱 적용.
 * @author shinys
 */
@Component
@Aspect
@Order(value = Ordered.LOWEST_PRECEDENCE)
public class CacheAspect {
    private static final Logger logger = LogManager.getLogger(CacheAspect.class);
    private CacheManager cacheManager;
    public CacheAspect(){
        this.cacheManager = new CacheManager();
    }

    /**
     * case 2. Data Loading and Reloading
     * 	Cache Miss가 발생하면 적절한 시점에 cache는 스스로 해당 부분의 데이터를 원본 데이터베이스로부터 reloading한다.
     *
     * @param joinPoint
     * @param cache
     * @return
     * @throws Throwable
     */
    @Around("@annotation(cache)")
    public Object cacheProcess(ProceedingJoinPoint joinPoint, Cache cache)throws Throwable{

        String key;
        if("".equals(cache.key())){
            key = joinPoint.getSignature().getName().concat( ":" )
                    .concat(Arrays.stream(joinPoint.getArgs()).map(obj->obj.toString()).collect(Collectors.joining(":")));
        }else{
            key = cache.key();
        }

        Object cached = cacheManager.get(key);
        if(cached!=null)
            return cached;

        Object obj = joinPoint.proceed();
        if(obj != null)
            cacheManager.put(key,obj,cache.ttl());

        return obj;
    }

    CacheManager getCacheManager(){
        return this.cacheManager;
    }

}
