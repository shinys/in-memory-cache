package com.shinys.cache;

import java.lang.annotation.*;

/**
 * Cache 애너테이션
 * @author shinys
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cache {
    /**
     * 캐시키
     * @return
     */
    String key() default "";

    /**
     * 캐시 보관 주기. 밀리세컨드.
     * @return
     */
    long ttl() default 60000L;
}
