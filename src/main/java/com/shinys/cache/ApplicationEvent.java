package com.shinys.cache;

import com.shinys.commerce.data.ICategoryRepository;
import com.shinys.commerce.data.IProductRepository;
import com.shinys.commerce.data.entities.Category;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * case 2. Data Loading and Reloading
 * 	캐시는 초기 구동시에 데이터를 원본 데이터베이스로부터 loading한다.
 */
@Component
@RequiredArgsConstructor
public class ApplicationEvent {
    private static final Logger logger = LogManager.getLogger(ApplicationEvent.class);


    private final IProductRepository iProductRepository;
    private final ICategoryRepository iCategoryRepository;
    private final CacheAspect cacheAspect;

    @EventListener(ApplicationReadyEvent.class)
    public void startup() {
        logger.info("ApplicationReady");
        logger.info("초기 데이터 캐시 적재");
        final long init = System.currentTimeMillis();
        CacheManager cacheManager = cacheAspect.getCacheManager();

        List<Category> categoryList = iCategoryRepository.findByDepth(1);
        cacheManager.put("categoryList", categoryList, 20000L);

        iProductRepository.findAll().forEach(p->{
            cacheManager.put("product".concat(":").concat(String.valueOf(p.getProductNo())), p,30000L);
        });

        logger.info("ApplicationReady take: {}ms", ()->(System.currentTimeMillis() - init));
    }
}
