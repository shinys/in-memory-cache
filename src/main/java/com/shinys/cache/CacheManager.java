package com.shinys.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.*;

/**
 * Simple In-Memory LRU Cache.
 * Thread-safe.
 * @author shinys
 */
class CacheManager {
    private static final Logger logger = LogManager.getLogger(CacheManager.class);
    private static ConcurrentHashMap<String,CacheObject> map;
    private static ScheduledExecutorService executorService;

    static {
        map = new ConcurrentHashMap<>();
        executorService = Executors
                .newSingleThreadScheduledExecutor();
    }

    CacheManager(){
        /**
         * case 3. Cache Data Eviction Policy
         * 	다양한 이유로 Cache 내부의 데이터는 Eviction 처리가 되어야 한다.
         * 	어떤 경우에 데이터 Eviction을 발생시킬 것인지를 정하고 코딩하라. 그리고 주석으로 그 이유를 서술하라.
         * 	어떤 데이터를 우선적으로 Eviction 시킬 것인지에 대한 정책을 정의하고 구현하여야 한다.
         * 	코드에 주석으로 왜 그런 정책을 사용했는지 서술하라.
         * 	(용어)
         * 	Eviction = LRU, MRU, FIFO, LIFO 등의 우선 순위로  구현 기법 (자유 구현)
         *
         * 캐시에서 객체를 조회하는 시점 'get(String key)' 에서도 단일 객체에 대한 만료 처리를 진행하지만
         * 스위퍼 쓰레드를 통해 만료되었으나 더 이상 hit 되지 않는 객체를 제거한다.
         *
         */
        executorService.scheduleAtFixedRate(
                ()->{
                    sweep();
                },
                2,
                10,
                TimeUnit.SECONDS
        );
    }

    /**
     * case 3. Cache Data Eviction Policy
     * 캐시에 적재된 데이터 조회 시점에 ttl 확인하여 만료된 객체는 제거하여 새로운 데이터를 적재할 수 있도록 한다.
     * @param key
     * @return
     */
    Object get(String key){
        CacheObject cache = map.get(key);

        if(cache==null) {
            logger.info("Cache missed!! key '{}'",()->key);
            return null;
        }

        if(cache.expired()){
            map.remove(key);
            logger.info("Cache expired!! key '{}'",()->key);
            return null;
        }

        logger.info("Cache hit!! key '{}'",()->key);
        return cache.element();
    }

    Object put(String key, Object ele, long ttl){
        map.put(key, new CacheObject(ele,ttl));
        return ele;
    }

    Object remove(String key){
        CacheObject obj = map.remove(key);
        if(obj==null)
            return null;

        return obj.element();
    }

    void sweep(){
        Set<String> expiredKeys = new TreeSet<>();
        map.forEach((k,v)->{
            if(v.expired())
                expiredKeys.add(k);
        });

        for(String k : expiredKeys){
            map.remove(k);
        }
        logger.info("Cache evict!! {} elements", expiredKeys.size());
    }

}
